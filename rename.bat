:: Batch rename all files, in alphabetical/numeric order, to
:: sequential numerical filenames with a pad of 4

@ECHO OFF
setlocal enabledelayedexpansion
PUSHD "%~1"

set inc=0

FOR /f "delims=" %%a in ('dir /b /a-d /O:N') DO (
    set /a inc+=1
	if !inc! LSS 1000 set zeros=0
	if !inc! LSS 100 set zeros=00
	if !inc! LSS 10 set zeros=000
    Echo Ren: "%%a" "!zeros!%~n1!inc!%%~xa"
    Ren "%%a" "!zeros!%~n1!inc!%%~xa"
)

POPD

