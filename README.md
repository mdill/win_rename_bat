# Windows Batch Rename

## Purpose

This is a batch/cmd file which can be run from within a working directory.

The purpose of this script is to rename all files within the directory to
four-digit, integer filenames.  As long as all files are in alpha-numeric order
prior to running the script, they should be renamed to sequential order.

## WARNING:

Windows is notorious for having bad naming and sorting conventions.  This means
that, even if the files appear to be in order in your file explorer, they may
not be in order within PowerShell.  In other words, sequential numbers in a
file explorer will appear thusly in PowerShell:

    01, 02, 03, ..., 08, 09, 10, 100, 101, 102, 103, ..., 11, 110, 111, 112, ...

This will cause your files to be renamed out of order.  It is good practice to
"pad" your filenames with zeroes to preserve their natural order.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/win_rename_bat.git

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/win_rename_bat/src/79fc5023e297f961ce96cc25dde67ae9e32ef81e/LICENSE.txt?at=master) file for
details.

